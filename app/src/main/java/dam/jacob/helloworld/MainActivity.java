package dam.jacob.helloworld;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends LogActivity {

    private final String DEBUG_TAG = "LogsAndroid-1";

    public MainActivity() {
        super.setDEBUG_TAG(this.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupUI();
    }

    private void setupUI(){
        Button btNextActivity = findViewById(R.id.btNextActivity);

        btNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, NextActivity.class));
            }
        });

    }

    public void launchNextActivity(View view){
        startActivity(new Intent(this, NextActivity.class));
    }


}